public class MyDate {
    int day, month, year;
    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String toString() {
        if(month <= 9 && day <= 9)
            return year + "-0" + month + "-0" + day;
        else if(day <= 9)
            return year + "-" + month + "-0" + day;
        else if(month <= 9)
            return year + "-0" + month + "-" + day;
        else
            return year + "-" + month + "-" + day;
    }

    void plusYear() {
        if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))
                && month==2 && day==29)
            day = 28;
        year++;
    }

    void incrementYear(int value) {
        if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))
                && month==2 && day==29 && value % 4 != 0)
            day = 28;
        year += value;
    }

    void minusYear() {
        if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))
                && month==2 && day==29)
            day = 28;
        year--;
    }
    void decrementYear(int value) {
        if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))
                && month==2 && day==29 && value % 4 != 0)
            day = 28;
        year -= value;
    }
    void incrementMonth() {
        if (month==12) {
            month = 1;
            plusYear();
        }

        else if (month==1 && (day==31 || day==30 || day==29)) {
            month++;
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
                day=29;
            else
                day=28;
        }

        else if (month!=7 && day==31) {
            month++;
            day=30;
            if (month==1 || month==3 || month==5 || month==7
                    || month==8 || month==10 || month==12)
                day=31;
        }

        else
            month++;
    }
    void incrementMonth(int value) {
        month += value;
        if (month > 12) {
            incrementYear(month / 12);
            month = month % 12;
        }

        if (day==31 && (month==4 || month==6 || month==9 || month==11))
            day=30;
        else if (month==2 && (day==31 || day==30 || day==29)) {
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
                day=29;
            else
                day=28;
        }
    }

    void decrementMonth() {
        if (month==1) {
            month = 12;
            minusYear();
        }

        else if (month==3 && (day==31 || day==30 || day==29)) {
            month--;
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
                day=29;
            else
                day=28;
        }

        else if (month!=8 && day==31) {
            month--;
            day=30;
            if (month==1 || month==3 || month==5 || month==7
                    || month==8 || month==10 || month==12)
                day=31;
        }

        else
            month--;
    }
    void decrementMonth(int value) {
        month -= value;
        if (month <= 0) {
            decrementYear((12 - month) / 12);
            month = Math.abs(12 + month) % 12 ;
        }
        if (day==31 && (month==4 || month==6 || month==9 || month==11))
            day=30;
        else if (month==2 && (day==31 || day==30 || day==29)) {
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
                day=29;
            else
                day=28;
        }
    }
    void incrementDay() {
        day++;
        if(day==32 || (day==31 && (month==4 || month==6 || month==9 || month==11))) {
            day = 1;
            incrementMonth();
        }
        else if (((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) && day==30 && month==2) {
            day = 1;
            incrementMonth();
        }
        else if (month==2 && day==29) {
            day = 1;
            incrementMonth();
        }
    }

    void incrementDay(int value) {
        for (int i=0; i<value; i++)
            incrementDay();
    }

    void decrementDay() {
        day--;
        if(day==0) {
            day = 31;
            decrementMonth();
        }
    }
    void decrementDay(int value) {
        for (int i=0; i<value; i++)
            decrementDay();
    }
    boolean isBefore(MyDate secondDate) {
        if (year < secondDate.year)
            return true;
        else if (month < secondDate.month)
            return true;
        else if (day < secondDate.day)
            return true;
        else
            return false;
    }
    boolean isAfter(MyDate secondDate) {
        if (year > secondDate.year)
            return true;
        else if (month > secondDate.month)
            return true;
        else if (day > secondDate.day)
            return true;
        else
            return false;
    }
    int dayDifference(MyDate secondDate) {
        int d1Sum = 0;
        int d2Sum = 0;
        int controlYear = 0;
        if (year == secondDate.year) {
            for (int i=0; i<month; i++) {
                if (i==4 || i==6 || i==9 || i==11)
                    d1Sum += 30;
                else if (i==1 || i==3 || i==5 || i==7 || i==8 || i==10 || i==12)
                    d1Sum += 31;
                else if (i==2 && ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))))
                    d1Sum += 29;
                else
                    d1Sum += 28;
            }
            for (int i=0; i< secondDate.month; i++) {
                if (i==4 || i==6 || i==9 || i==11)
                    d2Sum += 30;
                else if (i==1 || i==3 || i==5 || i==7 || i==8 || i==10 || i==12)
                    d2Sum += 31;
                else if (i==2 && ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))))
                    d2Sum += 29;
                else
                    d2Sum += 28;
            }
        }
        else
        if (year < secondDate.year) {
            controlYear = year;
            for (int i=0; i < secondDate.year - year; i++) {
                if (((controlYear % 400 == 0) || ((controlYear % 4 == 0) && (controlYear % 100 != 0))))
                    d1Sum += 366;
                else
                    d1Sum += 365;
                controlYear++;
            }
        }

        else
            controlYear = secondDate.year;
        for (int i=0; i < year - secondDate.year; i++) {
            if (((controlYear % 400 == 0) || ((controlYear % 4 == 0) && (controlYear % 100 != 0))))
                d2Sum += 366;
            else
                d2Sum += 365;
            controlYear++;
        }
        return Math.abs((d1Sum + day) - (d2Sum + secondDate.day));
    }
}