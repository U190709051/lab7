public class Tester {
    public static void main(String[] args) {
        Student student = new Student(34.1, 16.8, 22.8);

        System.out.println(student.toString());

        student.inPlaceSort();

        System.out.println(student.toString());

        Student sortedStudent = student.outPlaceSort();

        System.out.println(sortedStudent.toString());

        System.out.println(student == sortedStudent);

    }
}